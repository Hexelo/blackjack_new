package bj;

public class Card 
{
	private int cardValue;
	private String value, suit, valueAndSuit = null;
	
	
//	protected String[] deck = null;

	
//	private static String[] names = new String[] {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
//	private static String[] suits = new String[] {"diamonds", "hearts", "spades", "clubs"};

	
	public Card(String aValue, String aSuit)
	{
		this.value = aValue;
		this.suit = aSuit;

	}
	
	
	public @Override String toString()
	{
			
				valueAndSuit = value + " of " + suit;
//				System.out.println(nameAndSuit + ": "+ cardValue);
				return valueAndSuit;
	}
			
		
	
	
	
	
	public int getCardValue()
	{
		return cardValue;
	}
	
	public String getValue() {
		return value;
	}

	public String getSuit()
	{
		return suit;
	}
	
	public String getValueAndSuit()
	{
		return valueAndSuit;
	}
	
	
}
