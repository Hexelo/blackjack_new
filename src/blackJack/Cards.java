package blackJack;
import java.util.ArrayList;

import blackJack.Value;

public abstract class Cards
{

	private int cardValue;
	private Value value;
	private static ArrayList<Value> valueList = new ArrayList<Value>();


	public Cards()
	{
		valueList.add(Value.ACE);
		valueList.add(Value.TWO);
		valueList.add(Value.THREE);
		valueList.add(Value.FOUR);
		valueList.add(Value.FIVE);
		valueList.add(Value.SIX);
		valueList.add(Value.SEVEN);
		valueList.add(Value.EIGHT);
		valueList.add(Value.NINE);
		valueList.add(Value.TEN);
		valueList.add(Value.JACK);
		valueList.add(Value.QUEEN);
		valueList.add(Value.KING);
	}
	
	public void setCardValue()
	{
		switch(getValue())
		{
			case ACE:
				this.cardValue = 1;
				break;
			case TWO:
				this.cardValue = 2;
				break;
			case THREE:
				this.cardValue = 3;
				break;
			case FOUR:
				this.cardValue = 4;
				break;
			case FIVE:
				this.cardValue = 5;
				break;
			case SIX:
				this.cardValue = 6;
				break;
			case SEVEN:
				this.cardValue = 7;
				break;
			case EIGHT:
				this.cardValue = 8;
				break;
			case NINE:
				this.cardValue = 9;
				break;
			case TEN:
				this.cardValue = 10;
				break;
			case JACK:
				this.cardValue = 10;
				break;
			case QUEEN:
				this.cardValue = 10;
				break;
			case KING:
				this.cardValue = 10;
				break;
			default: System.out.println("Invalid card");
				return;

		}
		
	}
	
	public void setCardValue(Value value)
	{
		if(value.equals(Value.ACE))
		{
			this.cardValue = 11;
		}
	}
	
	public int getCardValue()
	{
		return cardValue;
	}
	

	public Value getValue() 
	{
		return value;
	}


	public void setValue(Value value) 
	{
		this.value = value;
	}
	
	public ArrayList<Value> getValueList()
	{
		return valueList;
	}
}


