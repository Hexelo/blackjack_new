package blackJack;

public class Deal extends Deck
	{
		private int cardCount = 52;
		private int points1;
		private int points2;
		private int turns;
		
		
		public void dealCards()
		{
			Deck myDeck = new Deck();
			myDeck.createCard();
			myDeck.shuffle();
//			myDeck.showCards();
			
			
			try
			{
				for(int j = 0; j < 5; j++)
				{

					for (int i = 0; i < 52; i+=2) 
					{
//						System.out.println(i);
						Cards deck = myDeck.getDeck().get(i);
						System.out.println("\nNy hand: \n" + deck.getValue()
								+ " of " + deck.getClass().getSimpleName()
								+ ": worth " + deck.getCardValue() + " points");
						points1 = deck.getCardValue();
						cardCount--;
						
			
							deck = myDeck.getDeck().get(i + 1);
							System.out.println(deck.getValue() + " of "
									+ deck.getClass().getSimpleName() + ": worth "
									+ deck.getCardValue() + " points");
							points2 = deck.getCardValue();
							cardCount--;
							turns++;
							System.out.println("Kort kvar i leken: " + cardCount);
							
							Hand hand = new Hand();
							hand.sumOfPoints();
						}
					
						if(cardCount == 0)
						{
							System.out.println("Slut p� kort! Blandar om leken.");

								myDeck.createCard();
								myDeck.shuffle();
								cardCount = 52;
						}
					
				}
				System.out.println("\n\nDu har nu spelat max antal omg�ngar (5 lekar), spelet �r slut.");
				Statistics stats = new Statistics();
				stats.statisticPerent();
			}
			catch(IndexOutOfBoundsException e)
			{
				e.printStackTrace();
			}
		}


		public int getPoints1() 
		{
			return points1;
		}
		
		public int getPoints2() 
		{
			return points2;
		}

		public int getTurns() 
		{
			return turns;
		}
		
}
