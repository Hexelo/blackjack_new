package blackJack;
import java.util.ArrayList;
import java.util.Collections;

public class Deck
{
//	private int cardCount = 52;
	private ArrayList<Cards> deck = new ArrayList<Cards>();
	
	public void addCardToList(Cards card)
	{
		deck.add(card);
	}
	
	public ArrayList<Cards> getDeck()
	{
		return deck;
	}
	
	public void createCard()
	{
		Cards card = null;

		for(int i = 0; i < 13; i++)
		{
			Diamonds diamonds = new Diamonds();
			diamonds.setValue(diamonds.getValueList().get(i));
			diamonds.setCardValue();
			card = diamonds;
			addCardToList(card);
			
			Hearts hearts = new Hearts();
			hearts.setValue(hearts.getValueList().get(i));
			hearts.setCardValue();
			card = hearts;
			addCardToList(card);
	
			Spades spades = new Spades();
			spades.setValue(spades.getValueList().get(i));
			spades.setCardValue();
			card = spades;
			addCardToList(card);
			
			Clubs clubs = new Clubs();
			clubs.setValue(clubs.getValueList().get(i));
			clubs.setCardValue();
			card = clubs;
			addCardToList(card);

		}	
	}
	
	public void showCards() 
	{
        for(Cards c : deck) 
        {
            deck.get(0);
        	System.out.println(c.getValue() + " of " + c.getClass().getSimpleName() + ": worth " + c.getCardValue() + " points");
        }
    }
	
	public void shuffle() 
	{
        Collections.shuffle(deck);
	}
}
